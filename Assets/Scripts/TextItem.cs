using System;

using UnityEngine;
using UnityEngine.UI;

public class TextItem: MonoBehaviour
{
    [SerializeField]
    private Text _text;
    [SerializeField]
    private Button _button;

    public event Action OnClick;

    private void Start() {
        _button.onClick.AddListener(_OnClicked);
    }

    private void OnDestroy() {
        _button.onClick.RemoveListener(_OnClicked);
    }

    public void SetText(string str) {
        _text.text = str;
    }

    private void _OnClicked() {
        if (OnClick != null) OnClick();
    }
}