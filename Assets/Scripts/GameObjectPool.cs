using UnityEngine;
using System.Collections.Generic;

public class GameObjectPool<T> where T:MonoBehaviour {
    private T _obj;
    private Queue<T> _availableList = new Queue<T>();
    private HashSet<T> _nonavailableSet = new HashSet<T>();
    public GameObjectPool(T obj) {
        _obj = obj;
    }
    public T CreateOrReuse(Transform parent, Vector2 position) {
        if (_AtLeastOneCanBeReused())
            return _Reuse(parent, position);

        var obj = GameObject.Instantiate<T>(_obj, Vector3.zero, Quaternion.identity, parent);
        _SetObject(obj.gameObject, parent, position);
        _nonavailableSet.Add(obj);

        return obj;
    }

    public bool Release(T obj) {
        if (_IsReleased(obj)) return false;

        _nonavailableSet.Remove(obj);
        _availableList.Enqueue(obj);
        return true;
    }

    public void ReleaseAll() {
        foreach (var obj in _nonavailableSet)
            Release(obj);
    }

    private bool _AtLeastOneCanBeReused() {
        return _availableList.Count > 0;
    }

    private bool _IsReleased(T obj) {
        return !_nonavailableSet.Contains(obj);
    }

    private T _Reuse(Transform parent, Vector2 position) {
        var obj = _availableList.Dequeue();
        _nonavailableSet.Add(obj);

        _SetObject(obj.gameObject, parent, position);

        return obj;
    }

    private void _SetObject(GameObject obj, Transform parent, Vector2 position) {
        var rectTransform = obj.GetComponent<RectTransform>();
        rectTransform.SetParent(parent);
        rectTransform.anchoredPosition = position;
    }
}