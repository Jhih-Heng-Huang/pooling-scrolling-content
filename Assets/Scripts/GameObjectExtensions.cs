using UnityEngine;

public static class GameObjectExtensions {
	public static T Instantiate<T>(this T original, Vector2 position, Transform parent) where T:MonoBehaviour {
		return GameObject.Instantiate<T>(original, new Vector3(position.x, position.y, 0f), Quaternion.identity, parent);
	}
}