﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

using Rayark.Mast;

public class PoolingContent : MonoBehaviour
{
	[SerializeField]
	private ScrollRect _scrollRect;
	[SerializeField]
	private GameObject _content;
	[SerializeField]
	private TextItem _textItem;

	private Executor _main = new Executor();

	private void Start() {
		_main.Add(_run());
	}

	private void Update() {
		if (!_main.Empty)
			_main.Resume(Rayark.Mast.Coroutine.Delta);
	}

	private IEnumerator _run() {
		TextItemInfo[] dataList = new string[] { "apple 1", "apple 2", "apple 3", "apple 4", "apple 5", "apple 6",
			"apple 7", "apple 8", "apple 9", "apple 10", "apple 11",}.Select(str => new TextItemInfo{ Text = str }).ToArray();

		var contentSetter = new ContentSetter<TextItemInfo, TextItem>(_scrollRect, _content, _textItem, _Combine);
		contentSetter.NumItemsToShow = 4;
		contentSetter.SpaceSize = 10;
		contentSetter.SetContent(dataList);
		while (true)
			yield return null;
	}

	private Defer _Combine(TextItemInfo data, TextItem obj) {
		obj.SetText(data.Text);
		Action onclick = () => {
			Debug.LogFormat("Item: {0}", data.Text);
		};
		obj.OnClick += onclick;

		var defer = new Defer();
		defer.Add(() => {
			obj.OnClick -= onclick;
		});
		return defer;
	}
}
