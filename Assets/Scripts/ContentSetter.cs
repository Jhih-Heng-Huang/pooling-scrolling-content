using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;

using Rayark.Mast;

public class ContentSetter<TData, TObject>
    where TObject: MonoBehaviour {
    private class ContentInfo {
        public GameObject GameObject;
        public Transform Transform;
        public RectTransform RectTransform;
    }

    private class ItemInfo {
        public TObject Object;
        public RectTransform RectTransform;
        public Rect Rect;
    }

    private ScrollRect _scrollRect;
    private ContentInfo _contentInfo = new ContentInfo();
    private ItemInfo _itemInfo;
    private Func<TData, TObject, Defer> _combine;
    private TData[] _dataList = null;
    private GameObjectPool<TObject> _pool = null;
    private Dictionary<TObject, Defer> _releaseDic = new Dictionary<TObject, Defer>();
    private Dictionary<int, TObject> _indexDic = new Dictionary<int, TObject>();
    private int _lastIndex;

    public int SpaceSize {
        get;
        set;
    }

    public int NumItemsToShow {
        get;
        set;
    }

    public ContentSetter(
        ScrollRect scrollRect,
        GameObject content,
        TObject item,
        Func<TData, TObject, Defer> combine) {

        _scrollRect = scrollRect;
        _contentInfo = _GenContentInfo(content);
        _itemInfo = _GenItemInfo(item);
        _pool = new GameObjectPool<TObject>(item);
        _combine = combine;

        SpaceSize = 5;
        NumItemsToShow = 5;

        _lastIndex = 0;
        _RegisterEvents();
    }

    ~ContentSetter() {
        _UnregisterEvents();
    }

    public void SetContent(TData[] list) {
        _ReleaseAllItems();
        _dataList = list;

        _MoveContentToTop();
        _SetContentSize(list.Length, _itemInfo.Rect.size);

        foreach (var index in Enumerable.Range(0, NumItemsToShow)) {
            _GenItem(index, list[index]);
        }
    }

    private void _ReleaseAllItems() {
        foreach (var defer in _releaseDic.Values) {
            defer.Dispose();
        }
        _pool.ReleaseAll();
    }

    private ContentInfo _GenContentInfo(GameObject content) {
        var result = new ContentInfo();
        result.GameObject = content;
        result.Transform = content.GetComponent<Transform>();
        result.RectTransform = content.GetComponent<RectTransform>();
        return result;
    }

    private ItemInfo _GenItemInfo(TObject item) {
        var result = new ItemInfo();
        result.Object = item;
        result.RectTransform = item.gameObject.GetComponent<RectTransform>();
        result.Rect = result.RectTransform.rect;
        _SetItemAnchorAndPivot(result.RectTransform);
        return result;
    }

    private bool _GenItem(int index, TData data) {
        if (_indexDic.ContainsKey(index)) return false;

        var obj = _pool.CreateOrReuse(_contentInfo.Transform, _GetPos(index));
        var defer = _combine(data, obj);
        _releaseDic.Add(obj, defer);
        _indexDic.Add(index, obj);
        return true;
    }

    private Vector2 _GetPos(int index) {
        return new Vector2(0, -_itemInfo.Rect.height * index - SpaceSize * index);
    }

    private bool _RemoveItem(int index) {
        if (!_indexDic.ContainsKey(index)) return false;

        var obj = _indexDic[index];
        var defer = _releaseDic[obj];
        defer.Dispose();
        _indexDic.Remove(index);
        _releaseDic.Remove(obj);
        _pool.Release(obj);
        return true;
    }

    private void _SetItemAnchorAndPivot(RectTransform rectTransform) {
        rectTransform.anchorMax = new Vector2(0.5f, 1f);
        rectTransform.anchorMin = new Vector2(0.5f, 1f);
        rectTransform.pivot = new Vector2(0.5f, 1f);
    }

    private void _RegisterEvents() {
        _scrollRect.onValueChanged.AddListener(_UpdateContent);
    }

    private void _UnregisterEvents() {
        _scrollRect.onValueChanged.RemoveListener(_UpdateContent);
    }

    private void _SetContentSize(int num, Vector2 itemSize) {
        var newSize = _contentInfo.RectTransform.sizeDelta;
        newSize.y = itemSize.y * num + SpaceSize * (num-1);
        _contentInfo.RectTransform.sizeDelta = newSize;
    }

    private void _MoveContentToTop() {
        _scrollRect.verticalNormalizedPosition = 1f;
    }

    private void _UpdateContent(Vector2 pos) {
        if (_dataList == null || _dataList.Length == 0)
            return;

        // Update Content
        var contentIndex = _VerticalNormalizedPosition2ContentIndex(_scrollRect.verticalNormalizedPosition);
        _UpdateContent(contentIndex);
    }

    private void _UpdateContent(int contentIndex) {
        if (_lastIndex == contentIndex) return;

        var newIndexSet = new HashSet<int>(_ContentIndex2ItemIndices(contentIndex));
        var oldIndexSet = new HashSet<int>(_ContentIndex2ItemIndices(_lastIndex));
        foreach (var itemIndex in oldIndexSet)
            if (!newIndexSet.Contains(itemIndex))
                _RemoveItem(itemIndex);
        

        foreach (var itemIndex in newIndexSet)
            if (!_indexDic.ContainsKey(itemIndex)) {
                _GenItem(itemIndex, _dataList[itemIndex]);
            }

        _lastIndex = contentIndex;
    }

    private int[] _ContentIndex2ItemIndices(int contentIndex) {
        return Enumerable.Range(contentIndex, NumItemsToShow).ToArray<int>();
    }

    private int _VerticalNormalizedPosition2ContentIndex(float pos) {
        if (NumItemsToShow >= _dataList.Length) return 0;

        var numIndex = _dataList.Length - NumItemsToShow + 1;
        var index = (int) Math.Floor((1 - pos) * numIndex);
        return Mathf.Clamp(index, 0, numIndex-1);
    }
}