# Pooling Scrolling Content

目的
-------------------------------------------------------------------

為了能夠動態新增和刪除 selector 上面的 items 所設計的 UI element

問題
-------------------------------------------------------------------

原先的設計是用 template 去實作，如下設計

```csharp=
public interface IItem<TData>
{
    void SetItem(TData data);
}

public class ContentSetter<TData>
{
    private GameObject _itemObject;
    private Pool<GameObject> _pool;

    public void SetContent(TData[] list);
}
```

由於繼承 monobehaviour 無法使用 template，因此，就透過其他方式設計

解法
-------------------------------------------------------------------

由於無法實踐 `ContentSetter<TData>`，所以就將 `Pool<T>` 和 `TData[]` 結合，如下：

```csharp=
public interface IArrayPool<T>
{
    public T Get(int index);
    public bool Recycle(int index);
    public void Clear();
}

public class ContentSetter
{
    private IArrayPool<GameObject> _arrayPool;
    
    public SetContent(IArrayPool<GameObject> pool);
}
```

這樣就不會有無法設定 template 的問題了。

現在想到，如果 `ContentSetter` 不繼承 Monobehaviour，不就可以定義 template 了?

因此，大致實作如下：

### ContentSetter.cs
```csharp=
public class ContentSetter<TData, TObject> where TObject: Monobehaviour {

    public int SpaceSize {
        get;
        set;
    }

    public int NumItemsToShow {
        get;
        set;
    }
    
    public void SetContent(TData[] list) {}

}
```

### GameObjectPool.cs
```csharp=
public class GameObjectPool<T>: where T: Monobehaviour {
    public GameObjectPool(T obj);

    public T CreateOrReuse();
    public bool Release(T obj);
    public bool ReleaseAll();
}
```

使用
-------------------------------------------------------------------

使用方法可以參考 `PoolingContent.cs` 的 `_run()`，看如何操作 `ContentSetter<TData, TObject>`。

```csharp=
private IEnumerator _run() {
    TextItemInfo[] dataList = new string[] { "apple 1", "apple 2", "apple 3", "apple 4", "apple 5", "apple 6",
        "apple 7", "apple 8", "apple 9", "apple 10", "apple 11",}.Select(str => new TextItemInfo{ Text = str }).ToArray();

    var contentSetter = new ContentSetter<TextItemInfo, TextItem>(_scrollRect, _content, _textItem, _Combine);
    contentSetter.NumItemsToShow = 4;
    contentSetter.SpaceSize = 10;
    contentSetter.SetContent(dataList);
    while (true)
        yield return null;
}

private Defer _Combine(TextItemInfo data, TextItem obj) {
    obj.SetText(data.Text);
    Action onclick = () => {
        Debug.LogFormat("Item: {0}", data.Text);
    };
    obj.OnClick += onclick;

    var defer = new Defer();
    defer.Add(() => {
        obj.OnClick -= onclick;
    });
    return defer;
}
```